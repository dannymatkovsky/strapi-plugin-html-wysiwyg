# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.5] - 2021-02-19

### Added

- Fixed bug when `value` prop is populated after first render.

## [1.0.4] - 2020-11-09

### Added

- Added missing leafs serialization and deserialization

## [1.0.3] - 2020-10-23

### Added

- Fixed broken iframe serialization
- Upgrade note in README

## [1.0.2] - 2020-10-23

### Added

- Fixed list tags deserialization

## [1.0.1] - 2020-10-19

### Added

- Undo/redo functionality
- Changelog
- Better README file

## [1.0.0] - 2020-10-08

### Added

- First stable version of plugin
